import matplotlib.pyplot as plt
import numpy as np
from physics_of_life.progressBar import printProgressBar


b = 100
d = 2
dt = 0.001
tmax = 10
t = 0
n = 0
plt.figure()
trajectory = [[n]]

printProgressBar(0, tmax, prefix='Progress:', suffix='Complete', length=50)

while t < tmax:
    if np.random.random() < b * dt:
        n += 1
    if np.random.random() < n * d * dt:
        n -= 1
    t += dt
    trajectory.append([n])
    plt.plot(trajectory)

    printProgressBar(t + dt, tmax, prefix='Progress:', suffix='Complete', length=50)

mean = np.mean(trajectory)

std = np.std(trajectory, dtype=np.float64)

plt.plot([0, 10000], [mean, mean], 'k-', lw=2)
plt.plot([0, 10000], [mean + std, mean + std], 'k-', lw=1, color='red')
plt.plot([0, 10000], [mean - std, mean - std], 'k-', lw=1, color='red')
plt.xlabel('time', fontsize=16)
plt.ylabel('proteins', fontsize=16)
plt.tick_params(labelsize=12)
plt.savefig('phipsi_graph.png')
plt.show()
plt.close()
