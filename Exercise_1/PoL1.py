import numpy as np
import matplotlib.pylab as plt
from physics_of_life.progressBar import printProgressBar

b = 50  # ten proteins are produced by unit of time
d = 2  # life time is one
dt = 0.001  # simulation time step

tmax = 100  # total time to simulate

narray = []

printProgressBar(0, 1000, prefix='Progress:', suffix='Complete', length=50)

for i in range(1, 1000):
    t = 0
    n = 0
    trajectory = [[t, n]]  # list to store all the results, starting with initial condition

    while t < tmax:
        # determine whether this is production event
        if np.random.random() < b * dt:
            n += 1  # add one
        if np.random.random() < n * d * dt:
            n -= 1  # remove one
        t += dt
        trajectory.append([t, n])


    narray.append(n)

    printProgressBar(i + 1, 1000, prefix='Progress:', suffix='Complete', length=50)


mean = np.mean(narray)

std = np.std(narray, dtype=np.float64)

plt.plot(narray)
plt.plot([0, 1000], [mean, mean], 'k-', lw=2)
plt.plot([0, 1000], [mean + std, mean + std], 'k-', lw=1, color='red')
plt.plot([0, 1000], [mean - std, mean - std], 'k-', lw=1, color='red')
plt.ylabel('some numbers')
plt.savefig('adi_graph.png')
plt.show()
