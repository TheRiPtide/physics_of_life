import numpy as np

D = 6E-10  #square meter per second
A = 5E-10**2 * np.pi  #square meters
L = 5E-9  #meters
dC = 0.001  #moles per cubic meter

J = D * (dC / L) * A

print(J)

J = J * 6.022E+23

print(J)


