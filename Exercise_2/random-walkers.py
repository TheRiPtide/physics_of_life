import numpy as np
import matplotlib.pyplot as plt

for p in [0.5, 0.53, 0.6]:

    # number of random walks to plot and number of steps each
    n_repetitions = 1000
    n_steps = 100
    # parameters of the random walk
    q = 1.0 - p

    # this list will contain all the trajectories we generate.
    all_traj = []
    # generate and plot several trajectories
    for i in range(n_repetitions):
        trajectory = [0]
        for j in range(100):
            right_or_left = 1 if np.random.random()<p else -1
            trajectory.append(trajectory[-1] + right_or_left)
        all_traj.append(trajectory)

    all_stat_vals = []

    for n in np.arange(10, 110, 20):
        position_after_n_steps = [traj[n] for traj in all_traj]
        expected_mean = n * (p - q)
        calculated_mean = np.mean(position_after_n_steps)
        expected_variance = 4 * n * p * q
        calculated_variance = np.var(position_after_n_steps)

        all_stat_vals.append([n, expected_mean, calculated_mean, expected_variance, calculated_variance])

        plt.hist(position_after_n_steps, label="$n={}$ steps".format(n), alpha=0.5, bins=np.arange(-30,50,2))

    plt.xlabel('position', fontsize=16)
    plt.tick_params(labelsize=12)
    plt.legend(fontsize=12, loc=2)
    plt.xlim(-30, 30)
    prob = p * 100
    plt.savefig('histogram_for_p_%d.png' % prob)
    plt.show()
    plt.close()

    barWidth = 0.3

    n_vals = [stats[0] for stats in all_stat_vals]
    exp_mean = [stats[1] for stats in all_stat_vals]
    exp_var = [stats[3] for stats in all_stat_vals]
    cal_mean = [stats[2] for stats in all_stat_vals]
    cal_var = [stats[4] for stats in all_stat_vals]

    xpos1 = np.arange(len(n_vals))
    xpos2 = [x + barWidth for x in xpos1]

    plt.bar(xpos1, exp_mean, width=barWidth, color=(0.3, 0.5, 0.4, 0.6), capsize=7, label='Statistical Values')

    plt.bar(xpos2, cal_mean, width=barWidth, color=(0.3, 0.9, 0.4, 0.6), capsize=7, label='Calculated Values')
    n_vals = ['n = ' + str(n_val) for n_val in n_vals]
    plt.xticks([r + barWidth/2 for r in range(len(n_vals))], n_vals)
    plt.ylabel('mean position')
    plt.legend()


    plt.savefig('mean_values_p_%d.png' % prob)
    plt.show()
    plt.close()

    plt.bar(xpos1, exp_var, width=barWidth, color=(0.3, 0.1, 0.4, 0.6), capsize=7, label='Statistical Values')

    plt.bar(xpos2, cal_var, width=barWidth, color=(0.3, 0.3, 0.4, 0.6), capsize=7, label='Calculated Values')
    n_vals = ['n = ' + str(n_val) for n_val in n_vals]
    plt.xticks([r + barWidth/2 for r in range(len(n_vals))], n_vals)
    plt.ylabel('variance')
    plt.legend()


    plt.savefig('variance_p_%d.png' % prob)
    plt.show()
    plt.close()