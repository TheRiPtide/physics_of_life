import numpy as np
import matplotlib.pyplot as plt
# number of random walks to plot and number of steps each
parr = [0.5, 0.53, 0.6, 0.9]
n_repetitions = 1000
n_steps = 100
# parameters of the random walk

for p in parr:
    means = []
    q = 1.0 - p
    a = []
    plt.figure()
    for i in range(n_repetitions):
        trajectory = [0]
        for j in range(101):
            right_or_left = 1 if np.random.random()<p else -1
            trajectory.append(trajectory[-1] + right_or_left)
        plt.plot(trajectory, alpha=0.2)
        a.append(trajectory)

    a = np.array(a)
    for k in range(11):
        means.append([(k*10),
                      a[:, k*10].mean(),
                      a[:, k*10].var(),
                      a[:, k*10].std()
                      ])
    # label the axis
    plt.xlabel('steps', fontsize=16)
    plt.ylabel('position, p = '+str(p), fontsize=16)
    plt.tick_params(labelsize=12)

    np_mean = np.array(means)
    x = np_mean[:, 0]
    y = np_mean[:, 1:]
    plt.scatter(x, y[:, 0], label='mean, at step 100 =  {:.2f}, calculated mean = {:.2f}'.format(y[-1, 0], n_steps*(p-q)))
    plt.scatter(x, y[:, 1], label='variance, at step 100 =  {:.2f}, calc var = {:.2f}'.format(y[-1, 1], 4*n_steps*p*q))
    plt.scatter(x, y[:, 2], label='std dev, at step 100 =  {:.2f}, calc std dev =  {:.2f}'.format(y[-1, 2], 2*np.sqrt(n_steps*p*q)))
    plt.legend()
    plt.savefig('random_walk_'+str(p)+'.png')
    plt.show()




